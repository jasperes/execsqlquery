# -*- coding: iso-8859-1 -*-

from os.path import exists
import pyodbc
import pymssqlce
import cx_Oracle
import kinterbasdb
#import adodbapi
from Crypto.Cipher import DES3
from Crypto import Random
import base64


class Main():
    """ """

    def __init__(self):
        """ """

        print "Verificando script"
        self.script = self.Decrypt()

        print "Abrindo arquivos de configuracao"
        if exists("conexao_db.ini"):
            cnfg = open("conexao_db.ini", "r")
            cfgT = True
        elif exists("bc_config.ini"):
            cnfg = open("bc_config.ini", "r")
            cfgT = False
        else:
            err = "Arquivo de configuracoes nao encontrado!"
            err += "\n"
            err += "Precione qualquer tecla para sair..."
            raw_input(err)
            sys.exit

        for line in cnfg.readlines():
            if line[:10].upper() == "TIPO_BANCO":
                tipo = line[12:].strip()

        if cfgT is True:
            print "Setando configuracao..."
            tipo = cnfg.readline()[12:].strip()
            host = cnfg.readline()[10:].strip()
            auth = cnfg.readline()[32:].strip()
            user = cnfg.readline()[9:].strip()
            pasw = cnfg.readline()[7:].strip()
            data = cnfg.readline()[12:].strip()
            del auth
        elif cfgT is False:
            print "Setando configuracao..."
            if tipo.upper() == "ACCESS":
                pass
            elif tipo.upper() == "SQL SERVER":
                host = "localhost\sqlexpress"
                user = "sa"
                #pasw = "_43690"
                data = "PontoSecullum4"
            elif tipo.upper() == "ORACLE":
                pass
            else:
                err = "Tipo de banco nao acessivel."
                err += "\n"
                err += "Por favor, verifique com o suporte"
                raw_input(err)
                exit()
        else:
            err = "Tipo de banco nao acessivel."
            err += "\n"
            err += "Por favor, verifique com o suporte"
            raw_input(err)
            exit()

        if tipo.upper() == "ACCESS":
            self.Access()
        elif tipo.upper() == "SQL SERVER CE":
            self.SQLCE()
        elif tipo.upper() == "SQL SERVER":
            self.SQLServer(host, user, "_43690", data)
        elif tipo.upper() == "FIREBIRD":
            self.Firebird(host, user, pasw, data)
        elif tipo.upper() == "ORACLE":
            self.Oracle()
        else:
            raw_input("Nada a fazer...")
            exit()

    def Decrypt(self, script=''):
        """ """

        try:
            des3 = DES3.new('espermikhordever', DES3.MODE_ECB,
                bytes('12345678'))
            with open('script', 'r') as in_file:
                while True:
                    chunk = in_file.read(16384)
                    if len(chunk) == 0:
                        break
                    chunk = base64.decodestring(chunk)
                    chunk = des3.decrypt(chunk)
                    script += chunk
        except:
            raw_input("Falha na execucao. Consulte o suporte.(Erro 69)")
            exit()

        return script

    def SQLServer(self, host, user, pssw, data):
        """ """

        try:
            print "Conectanto com o banco de dados..."
            conStr = 'DRIVER={SQL Server};'
            conStr += 'SERVER='
            conStr += host
            conStr += ';DATABASE='
            conStr += data
            conStr += ';UID='
            conStr += user
            conStr += ';PWD='
            conStr += pssw
            self.conn = pyodbc.connect(conStr)
            print "Conectado!"
        except:
            print "Falha ao conectar com o banco de dados..."
            print "Por favor, tente novamente:"
            host = raw_input("Servidor\Instancia: ")
            user = raw_input("Usuario: ")
            pssw = raw_input("Senha: ")
            data = raw_input("Banco de dados: ")
            try:
                print "Conectanto com o banco de dados..."
                conStr = 'DRIVER={SQL Server};'
                conStr += 'SERVER='
                conStr += host
                conStr += ';DATABASE='
                conStr += data
                conStr += ';UID='
                conStr += user
                conStr += ';PWD='
                conStr += pssw
                self.conn = pyodbc.connect(pssw)
                print "Conectado!"
            except:
                print "Falha ao conectar com o banco de dados!"
                print "Precione qualquer tecla para finalizar..."
                raw_input()
                exit()

        self.cur = self.conn.cursor()
        self.Execute()

    def Access(self):
        """ """

        try:
            print "Conectanto com o banco de dados..."
            conStr = "Driver={Microsoft Access Driver (*.mdb)};"
            conStr += "Dbq=psec.dat;Uid=;Pwd=;"
            self.conn = pyodbc.connect(conStr)
            print "Conectado!"
        except:
            print "Falha ao conectar com o banco de dados!"
            print "Precione qualquer tecla para finalizar..."
            raw_input()
            exit()

        self.cur = self.conn.cursor()
        self.Execute()

    def Oracle(self, host, user, pssw):
        """ """

        try:
            print "Conectanto com o bano de dados..."
            conStr = user
            conStr += "/"
            conStr += pssw
            conStr += "@"
            conStr += host
            con = cx_Oracle.connect(conStr)
            print "Conectado!"
        except:
            print "Falha ao conectar com o banco de dados!"
            print "Erro CX"
            print "Precione qualquer tecla para finalizar..."
            raw_input()
            exit()

        self.cur = self.conn.cursor()
        self.Execute()

    def Firebird(self, host, user, pssw, data):
        """ """

        try:
            print "Conectanto com o banco de dados..."
            self.conn = kinterbasdb.connect(host=host,
                database=data,user=user,password=pssw)
            print "Conectado!"
        except:
            print "Falha ao conectar com o banco de dados..."
            print "Por favor, tente novamente:"
            host = raw_input("Servidor: ")
            print "Conectanto com o banco de dados..."
            pssw = raw_input("Senha: ")
            data = raw_input("Banco de dados: ")
            try:
                print "Conectanto com o banco de dados..."
                self.conn = kinterbasdb.connect(host=host, database=data,
                    user=user, password=pssw)
                print "Conectado!"
            except:
                print "Falha ao conectar com o banco de dados!"
                print "Precione qualquer tecla para finalizar..."
                raw_input()
                exit()

        self.cur = self.conn.cursor()
        self.Execute()

    def SQLCE(self, data):
        """" """

        try:
            print "Conectado com o banco de dados..."
            self.conn = pymssqlce.connect(data)
            print "Conectado!"
        except:
            print "Falha ao executar"
            print "consulte o suporte..."
            raw_input()
            exit()

        self.cur = self.conn.cursor()
        self.Execute()

    def Execute(self):
        """ """

        try:
            print "Executando..."
            self.cur.execute(self.script)
            self.conn.commit()
            print "Comando executado com exito!"
            raw_input("Precione qualquer tecla para sair...")
        except:
            print "Falha ao executar comando!"
            raw_input("Precione qualquer tecla para sair...")

        exit()

if __name__ == '__main__':
    Main()